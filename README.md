# WorldWideMigrate

WorldWideMigrate is a platform that allows to simplify and extend migration processes both for governors and migrants. 

It allows governors to provide and migrants to accept proposals: a set of job and place for living that are free and desirable for government to get taken.

Also app will allow to register migrants' moves and financial actions in order to get clear statistics about migration processes and theirs financial efficiency. 

## Start an API server
``` cd backend && node index ```

## Start a governor's Nuxt client-server

``` cd governor && npm run dev ```

## Run users creation
``` cd tests &&& npx jest governors ```

## Auth creds
``` root\1111 ```