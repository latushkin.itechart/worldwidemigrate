import Api from "@/include/Api";
import proposals from "@/include/stub/proposals";

export default {

  state: () => ({
    user: {},
    auth: false,
    token: null,
    countries: [],
    proposals: [],
  }),

  mutations: {
    ['AUTH_USER'](state, payload) {
      state.user = payload;
      state.auth = true;
      state.token = payload.token;
      window.localStorage.setItem('app-token', payload.token);
    },

    ['GET_STORED_TOKEN'](state) {
      state.token = localStorage.getItem('app-token') || null;
    },

    ['LOGOUT'](state) {
      state.token = '';
      localStorage.setItem('app-token', null);
    },

    ['SAVE_COUNTRIES'](state, data) {
      state.countries = data;
    },

    ['SAVE_PROPOSALS'](state, data) {
      state.proposals = data || [];
    },

    ['APPEND_PROPOSAL'](state, item) {
      state.proposals.unshift(item);
    },

    ['REMOVE_PROPOSAL'](state, item) {
      state.proposals = state.proposals.filter(x => x !== item);
    },

    ['REPLACE_PROPOSAL'](state, item) {
      state.proposals.splice(state.proposals.findIndex(x => x.id === item.id), 1, item);
    }
  },

  actions: {

    async login(ctx, {login, password}) {
      const result = await Api.Auth.login(login, password);
      if (result.status === 'OK') {
        ctx.commit('AUTH_USER', result.data);
      }

      return result;
    },

    async pingUser(ctx) {
      ctx.commit('GET_STORED_TOKEN');
      Api.token = ctx.state.token;
      const result = await Api.Auth.ping();
      if (result.status === 'OK') {
        ctx.commit('AUTH_USER', result.data);
        return true;
      }
      return false;
    },

    async loadCountries(ctx) {
      Api.getCountries().then((result) => {
        console.log(result)
        ctx.commit('SAVE_COUNTRIES', result.data);
      })
    },

    async loadProposals(ctx) {

      const proposals = (await Api.Proposals.getList()).data;
      console.log(proposals);
      ctx.commit('SAVE_PROPOSALS', proposals);
    },

    async createProposal(ctx, item) {
      const createdItem = (await Api.Proposals.create(item)).data;
      ctx.commit('APPEND_PROPOSAL', createdItem);
      return createdItem;
    },

    async deleteProposal(ctx, item) {
      const result = (await Api.Proposals.delete({proposalId: item.id})).data;
      ctx.commit('REMOVE_PROPOSAL', item);

    },

    async updateProposal(ctx, item) {
      const resultItem = (await Api.Proposals.update(item)).data;
      ctx.commit('REPLACE_PROPOSAL', resultItem);
    }
  }
}

