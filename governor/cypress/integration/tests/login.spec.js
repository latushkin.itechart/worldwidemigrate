context('Login', () => {

  it('Should pass data in login and password inputs', () => {
    cy.visit('http://localhost:3000')

    cy.get('.email-field input').type('root').should('have.value', 'root');
    cy.get('.password-field input').type('1111').should('have.value', '1111');
    cy.get('.v-btn.primary').click();

    cy.wait(1000);
    cy.get('.v-toolbar__content').should('be.visible');
  })
})
