const n = (a, b) => Math.round(Math.random() * (b - a) + a);

export default () => {
  return ({
    timestamp: Date.now(),
    id: n(10 ** 10, 10 ** 11 - 1),
    title: new Array(n(3, 12)).fill('').map(() => new Array(n(5, 12)).fill('').map(() => String.fromCharCode(n(96, 122))).join('')).join(' '),
    payload: n(10 ** 8, 10 ** 9),
  });
}
