import axios from "axios";

const baseUrl = `http://localhost:1147/governor`;

const Api = {

  token: null,
  async execRequest(method, path, data = {}) {
    const dataToSend = {
      ...data,
      token: this.token
    };

    return (await axios.request({
      method,
      url: baseUrl + path,
      ...(['get', 'delete'].includes(method) ? {params: dataToSend} : {data: dataToSend})
    })).data;
  },

  get(path, data, ...args) { return this.execRequest('get', path, data, ...args); },
  post(path, data, ...args) { return this.execRequest('post', path, data, ...args); },
  put(path, data, ...args) { return this.execRequest('put', path, data, ...args); },
  delete(path, data, ...args) { return this.execRequest('delete', path, data, ...args); },

  Auth: {
    login: (login, password) => Api.post( '/auth/', {login, password}),
    ping: () => Api.post( '/ping/'),
  },

  Proposals: {
    getList: () => Api.get( '/proposals/'),
    create: (item) => Api.post( '/proposals/', item),
    delete: (item) => Api.delete('/proposals/', item),
    update: (item) => Api.put('/proposals/', item),
  },

  getCountries: () => Api.get( '/countries/'),
}

export default Api;
