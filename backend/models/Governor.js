const { Model } = require('sequelize');
const { DataTypes } = require('sequelize');
const Country = require('./Country');

const sequelize = require('../include/getSequelize')();

class Governor extends Model { };

Governor.init({
    name: DataTypes.STRING(100),
    login: DataTypes.STRING(100),
    password: DataTypes.STRING(100),
    token: DataTypes.TEXT,
}, {
    tableName: 'governors',
    sequelize
});

Governor.belongsTo(Country);

module.exports = Governor;