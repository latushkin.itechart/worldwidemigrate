const { Model } = require('sequelize');
const { DataTypes } = require('sequelize');
const Proposal = require('./Proposal');

const sequelize = require('../include/getSequelize')();

class Workplace extends Model {};

Workplace.init({
    jobTitle: DataTypes.STRING(200),
    companyName: DataTypes.STRING(200),
    address: DataTypes.STRING(200),
    photoUrl: DataTypes.STRING,
    requirements: DataTypes.TEXT,
    salary: DataTypes.INTEGER,
}, {
    sequelize,
});


module.exports = Workplace;