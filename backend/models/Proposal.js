const { Model } = require('sequelize');
const { DataTypes } = require('sequelize');
const Country = require('./Country');
const Workplace = require('./Workplace');
const Accommodation = require('./Accommodation');

const sequelize = require('../include/getSequelize')();

class Proposal extends Model {

    async fetch() {
        const item = this.toJSON();

        item.country = (await Country.findByPk(item.CountryId)).toJSON();
        delete item.CountryId;

        item.workplace = (await Workplace.findByPk(item.WorkplaceId)).toJSON();
        delete item.WorkplaceId;

        item.accommodation = (await Accommodation.findByPk(item.AccommodationId)).toJSON();
        delete item.AccommodationId;

        return item;
    }
};

Proposal.init({
    conditions: DataTypes.TEXT,
    documents: DataTypes.TEXT,
}, {
    tableName: 'proposals',
    sequelize
});

Proposal.belongsTo(Country);
Workplace.hasMany(Proposal);
Accommodation.hasMany(Proposal);

module.exports = Proposal;