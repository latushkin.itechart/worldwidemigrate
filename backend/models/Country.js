const { Model } = require('sequelize');
const { DataTypes } = require('sequelize');

const sequelize = require('../include/getSequelize')();

class Country extends Model {

}

Country.init({
    iso: {
        type: DataTypes.STRING(3)
    },
    name: {
        type: DataTypes.STRING(100)
    }
}, {
    sequelize,
    tableName: 'countries'
});

module.exports = Country;