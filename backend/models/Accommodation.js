const { Model, DataTypes } = require('sequelize');
const Proposal = require('./Proposal');
const sequelize = require('../include/getSequelize')();

class Accommodation extends Model {};

Accommodation.init({
    address: DataTypes.STRING(200),
    fee: DataTypes.INTEGER,
    utilities: DataTypes.INTEGER,
    conditions: DataTypes.TEXT,
}, {
    sequelize
});


module.exports = Accommodation;