const findUser = require("../include/findUser");

module.exports = async (req, res, next) => {
    const token = req.body.token || req.query.token;
    if (!token) {
        return res.json({status: 'ERROR', message: 'Auth token is not provided', code: 'no_token'});
    }

    const user = await findUser.byToken(token);
    if(!user) {
        return res.json({status: 'ERROR', message: 'Token is incorrect or expired!', code: 'incorrect_token'});
    }

    req.user = user.toJSON();
    next();
}