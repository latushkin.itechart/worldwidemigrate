const { Router } = require("express");
const getAuthorizedUser = require('./middleware/getAuthorizedUser');
const formatResponse = require("../include/formatResponse");
const Proposal = require("../models/Proposal");
const Accommodation = require("../models/Accommodation");
const Workplace = require("../models/Workplace");

const protectedRouter = new Router();
protectedRouter.use(getAuthorizedUser);

protectedRouter.post('/ping', (req, res) => {
    res.json(formatResponse.success(req.user));
});

protectedRouter.get('/proposals', async (req, res) => {
    const proposals = await Proposal.findAll({
        order: [['updatedAt', 'DESC']]
    });
    const result = [];
    for (const prop of proposals) {
        result.push(await prop.fetch())
    }
    res.json(formatResponse.success(result));
});

protectedRouter.post('/proposals', async (req, res) => {

    const data = req.body;
    const accommodation = data.accommodation;
    const workplace = data.workplace;

    const acc = await Accommodation.create({
        address: accommodation.address,
        fee: accommodation.fee,
        utilities: accommodation.utilities,
        conditions: accommodation.conditions,
        photoUrl: workplace.photoUrl
    });

    const wp = await Workplace.create({
        jobTitle: workplace.jobTitle,
        companyName: workplace.companyName,
        photoUrl: workplace.photoUrl,
        address: workplace.address,
        conditions: workplace.conditions,
        salary: workplace.salary,
    });

    const prop = await Proposal.create({
        CountryId: data.countryId,
        AccommodationId: acc.id,
        WorkplaceId: wp.id,
        conditions: data.conditions,
        documents: data.documents,
    });

    res.json(formatResponse.success(await prop.fetch()));
})

protectedRouter.delete('/proposals/', async (req, res) => {
    const proposalId = req.body.proposalId || req.query.proposalId;
    const proposal = await Proposal.findByPk(proposalId);
    const data = proposal.toJSON();

    await proposal.destroy();
    await (await Accommodation.findByPk(data.AccommodationId)).destroy()
    await (await Workplace.findByPk(data.WorkplaceId)).destroy();

    res.json(formatResponse.success({}));
});

protectedRouter.put('/proposals/', async (req, res) => {
    const workplace = req.body.workplace;
    const accommodation = req.body.accommodation;
    const d = req.body;

    const proposal = await Proposal.findByPk(req.body.id);
    proposal.CountryId = d.CountryId;
    proposal.conditions = d.conditions;
    proposal.documents = d.documents;
    await proposal.save();

    const wp = await Workplace.findByPk(workplace.id);
    delete workplace.id;
    for (let key in workplace) {
        wp[key] = workplace[key];
    }
    await wp.save();

    const acc = await Accommodation.findByPk(accommodation.id);
    delete accommodation.id;
    for(let key in accommodation) {
        acc[key] = accommodation[key];
    }
    await acc.save();

    res.json(formatResponse.success(await proposal.fetch()));

});

module.exports = protectedRouter;