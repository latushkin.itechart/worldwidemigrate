const Governor = require('../../models/Governor');

module.exports = {
    byLoginAndHash: async (username, passwordHash) => {
        return await Governor.findOne({
            where: {
                login: username,
                password: passwordHash
            }
        });        
    },

    byToken: async (token) => {
       return await Governor.findOne({
           where: {
               token
           }
       })
    }
}
