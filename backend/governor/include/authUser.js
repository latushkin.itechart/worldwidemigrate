const getHash = require('../../include/getHash');
const Governor = require('../../models/Governor');

module.exports = async ({id}) => {
    const token = getHash(Math.random() * 10 ** 8) + getHash(Math.random() * 10 ** 7);
    
    const user = await Governor.findByPk(id);
    
    user.token = token;
    await user.save({fields: ['token']});
    
    return token;
}