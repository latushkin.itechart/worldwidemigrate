const { Router } = require('express');
const getHash = require('../include/getHash');
const authUser = require('./include/authUser');
const findUser = require('./include/findUser');
const protectedRouter = require('./protected');
const formatResponse = require('../include/formatResponse');
const Country = require('../models/Country');


const router = Router();

router.post('/auth',  (req, res) => {

    const login = req.body.login;
    const password = req.body.password;
    const hash = getHash(password);

    findUser.byLoginAndHash(login, hash).then((user) => {
        if(user) {
            authUser(user).then((token) => {
                res.json(formatResponse.success({...user.toJSON(), token}));
            })
        } else {
            res.json(formatResponse.error('incorrect_credentials', 'Incorrect login or / and password'));
        }
    })
    
});

router.get('/countries', async (req, res) => {
    const countries = await Country.findAll();
    res.json(formatResponse.success(countries));
});


router.use(protectedRouter);
module.exports = router;