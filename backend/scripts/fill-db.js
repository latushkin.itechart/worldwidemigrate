const { Client } = require('pg');
const countriesList = require('countries-list');

const client = new Client({
    user: 'wwm',
    password: '1234567',
    database: 'worldwidemigrate',
});

client.connect().then(async () => {

    const tasks = Object.entries(countriesList.countries).map(([code, country]) => {
        const query = `INSERT INTO countries VALUES (DEFAULT, '${code}', '${country.name}')`;
        return client.query(query);
    });
    await Promise.all(tasks);

    const query = `SELECT * FROM countries`;
    const res = (await client.query(query)).rows;

    console.log(res);

    return client.end();
})