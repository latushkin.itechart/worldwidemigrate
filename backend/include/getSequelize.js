const Sequelize = require('sequelize');

const sequelize = new Sequelize(`postgres://wwm:1234567@localhost:5432/worldwidemigrate`, { logging: false});

module.exports = () => sequelize;