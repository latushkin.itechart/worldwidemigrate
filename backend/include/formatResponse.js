module.exports = {
    success(data) {
        return {
            status: 'OK',
            data
        }
    },

    error(code, message, data) {
        return {
            status: 'ERROR',
            message,
            code,
            data
        }
    }
}