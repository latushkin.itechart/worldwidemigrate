const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const governorRouter = require('./governor');

const app = express();
const PORT = 1147;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());

app.get('/', (req, res) => {
    res.send('WorldWide Migrate API');
});

app.use('/governor', governorRouter);

app.listen(PORT, () => {
    console.log(`Server listening at http://localhost:${PORT}`);
});