const Country = require("../models/Country")
const countriesList = require('countries-list');
const { Op } = require("sequelize");

test('Countries can be added to DB', async () => {
    await Country.sync({ force: true });
    const queue = Object.entries(countriesList.countries).map(async ([key, { name }]) => {
        const record = await Country.create({
            iso: key,
            name
        });
    });
    await queue;

});

test('Countries non-empty list can be received', async () => {
    const res = await Country.findAll();
    expect(res.length > 100).toBe(true);

    return true;
});

test('Countries by query can be received', async () => {
    const res = await Country.findAll({
        where: {
            id: {
                [Op.gte]: 110
            }
        }
    });

    expect(res.length > 100).toBe(true);
})