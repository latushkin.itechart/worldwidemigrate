const Governor = require("../models/Governor")
const getHash = require('../include/getHash');
const Country = require("../models/Country");

test('Governor can be created', async () => {
    await Governor.sync({force: true});
    const gov = await Governor.create({
        name: 'Main Governor',
        login: 'root',
        password: getHash('1111'),
        token: '',
        CountryId: (await Country.findByPk(146)).id
    });

    expect(gov instanceof Governor).toBe(true);
    expect(gov.id >= 1).toBe(true);

    return true;
})