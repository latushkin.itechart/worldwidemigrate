const Proposal = require("../models/Proposal");
const Accommodation = require("../models/Accommodation");
const Workplace = require("../models/Workplace");

test('Proposal with params can be added', async (done) => {
    await Proposal.sync({force: true});
    await Accommodation.sync({force: true});
    await Workplace.sync({force: true});

    

    

    const wp = await Workplace.create({
        jobTitle: 'Test job title',
        companyName: 'TestCompany',
        photoUrl: null,
        address: 'Test Street, 15',
        conditions: 'Test Job',
        salary: 100500,
    });

    const acc = await Accommodation.create({
        address: 'Test Street, 42',
        conditions: 'Test Accommodation conditions',
        fee: 100500,
        utilities: 10500,
    });

    const proposal = await Proposal.create({
        CountryId: 146,
        conditions: 'Test Proposal conditions',
        documents: 'Test Documents',
        AccommodationId: acc.id,
        WorkplaceId: wp.id,
    });

    // console.log(wp.toJSON());
    // console.log(acc.toJSON());
    // console.log(proposal.toJSON());

    const proposals = await Proposal.findAndCountAll();
    expect(proposals.count > 0).toBe(true);

    done();
}, 2000)

test('Full proposal data can be extracted', async (done) => {
    const proposal = await Proposal.findByPk(1);
    const fetched = await proposal.fetch();

    console.log(fetched);

    done();
})